#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX_SIZE 21

typedef struct film_st {
   char naziv[MAX_SIZE];
   float ocena;
   char zanr[MAX_SIZE];
   unsigned godina;
}FILM;

typedef struct cvor_st {
   FILM film;
   struct cvor_st *sledeci;
}CVOR;

FILE *safe_open(char *, char *, int);
void inicijalizacija(CVOR **);
void ucitavanje(FILE *, CVOR **);
void dodavanje(CVOR **, CVOR *);
void ispis_jednog(FILE *, FILM);
void ispis_liste(FILE *, CVOR *);
void brisanje_liste(CVOR **);
void min_max(FILE *,CVOR *);
void ispis_zanra(FILE *, CVOR *, char *);
void max_ocena_nakon_godine (FILE *, CVOR *, unsigned);


int main(int argc, char **argv) {
   if (argc != 4) {
      puts("Niste dobro pozvali program.");
      exit(EXIT_FAILURE);
   }
   
   CVOR *glava;
   inicijalizacija(&glava);
   FILE *in = safe_open(argv[1], "r", EXIT_FAILURE);
   ucitavanje(in, &glava);
   min_max(stdout, glava);
   char naziv_fajla[MAX_SIZE +9] = "zanr_";
   strcat(naziv_fajla, argv[3]);
   strcat(naziv_fajla, ".txt");
   FILE *out = safe_open(naziv_fajla, "w", EXIT_FAILURE);
   ispis_zanra(out, glava, argv[3]);
   fclose(out);
   unsigned zadata_godina = atoi(argv[2]);
   out = safe_open("izlaz.txt", "w", EXIT_FAILURE);
   max_ocena_nakon_godine(out, glava, zadata_godina);
   fclose(out);
   
   brisanje_liste(&glava);
   fclose(in);
   return 0;
}

FILE *safe_open(char *name, char *mode, int err_code) {
   FILE *fp = fopen(name, mode);
   if (fp == NULL) {
      printf("Doslo je do greske prilikom otvaranja %s datoteke.\n", name);
      exit(err_code);
   }
   return fp;
}

void inicijalizacija(CVOR **pglava) {
   *pglava = NULL;
}

void ucitavanje(FILE *in, CVOR **pglava) {
   FILM temp;
   while(fscanf(in, "%s %f %s %u", temp.naziv, &temp.ocena, temp.zanr, &temp.godina)!=EOF) {
      CVOR *novi = (CVOR *) malloc(sizeof(CVOR));
      novi->film = temp;
      novi->sledeci = NULL;
      dodavanje(pglava, novi);
   }
}
void dodavanje(CVOR **pglava, CVOR *novi) {
   if (*pglava == NULL) {
      *pglava = novi;
      return;
   }
   
   CVOR *tmp = *pglava;
   while(tmp->sledeci != NULL) {
      tmp = tmp->sledeci;
   }
   tmp->sledeci = novi;
}
void ispis_jednog(FILE *out, FILM temp) {
   fprintf(out, "%s %.1f %s %u\n", temp.naziv, temp.ocena, temp.zanr, temp.godina);
}  
void ispis_liste(FILE *out, CVOR *glava) {
   while(glava!=NULL) {
      ispis_jednog(out, glava->film);
      glava = glava->sledeci;
   }
}
void brisanje_liste(CVOR **pglava) {
   CVOR *za_brisanje;
   while(*pglava!= NULL) {
      za_brisanje=*pglava;
      *pglava = (*pglava)->sledeci;
      za_brisanje->sledeci = NULL;
      free(za_brisanje);
   }
}

void min_max(FILE *out,CVOR *glava) {
   FILM min_f = glava->film, max_f = glava->film;
   
   while(glava!=NULL) {
      if (min_f.godina > glava->film.godina) 
         min_f = glava->film;
      if (max_f.godina < glava->film.godina) 
         max_f = glava->film;
         
      glava = glava->sledeci;
   }
   
   fprintf(out, "Najstariji film je %s i objavljen je %u. godine, a najnoviji film je %s i objavljen je %u. godine.\n", min_f.naziv, min_f.godina, max_f.naziv, max_f.godina);
}

void ispis_zanra(FILE *out, CVOR *glava, char *zanr) {
   while(glava!=NULL) {
      if (strcmp(glava->film.zanr, zanr)==0)
         ispis_jednog(out, glava->film);
      glava = glava->sledeci;
   }
}

void max_ocena_nakon_godine (FILE *out, CVOR *glava, unsigned godina) {
   FILM max;
   CVOR *tmp = glava;
   
   while(tmp!=NULL) {
      if (tmp->film.godina > godina) {
         max = tmp->film;
         break;
      }
      tmp=tmp->sledeci;
   }
   
   while(glava!=NULL) {
      if (glava->film.godina > godina && max.ocena < glava->film.ocena) {
         max = glava->film;
      }
      glava=glava->sledeci;
   }
   
   ispis_jednog(out, max);
   
}
